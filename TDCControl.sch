EESchema Schematic File Version 2
LIBS:TDCombat-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:ThunderDrive
LIBS:crf_1
LIBS:con-amp-micromatch
LIBS:TDCombat-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Thunderdrive Combat"
Date "2016-02-15"
Rev "2"
Comp "Thunder Tecnologies"
Comment1 "STM32F446"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +3.3V-RESCUE-TDCombat #PWR04
U 1 1 56239354
P 9050 3400
F 0 "#PWR04" H 9050 3250 50  0001 C CNN
F 1 "+3.3V" H 9050 3540 50  0000 C CNN
F 2 "" H 9050 3400 60  0000 C CNN
F 3 "" H 9050 3400 60  0000 C CNN
	1    9050 3400
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-TDCombat C7
U 1 1 5623998F
P 9400 1400
F 0 "C7" V 9350 1450 50  0000 L CNN
F 1 "13p 6V" V 9450 1450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9438 1250 30  0001 C CNN
F 3 "" H 9400 1400 60  0000 C CNN
	1    9400 1400
	0    -1   1    0   
$EndComp
$Comp
L C-RESCUE-TDCombat C6
U 1 1 562399BB
P 9400 900
F 0 "C6" V 9350 950 50  0000 L CNN
F 1 "13p 6V" V 9450 950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9438 750 30  0001 C CNN
F 3 "" H 9400 900 60  0000 C CNN
	1    9400 900 
	0    -1   1    0   
$EndComp
$Comp
L CONN_02X05 P15
U 1 1 5624534D
P 9150 2450
F 0 "P15" H 9150 2750 50  0000 C CNN
F 1 "SWD" H 9150 2150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05_Pitch1.27mm" H 9150 1250 60  0001 C CNN
F 3 "" H 9150 1250 60  0000 C CNN
	1    9150 2450
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V-RESCUE-TDCombat #PWR05
U 1 1 56245B05
P 9500 2200
F 0 "#PWR05" H 9500 2050 50  0001 C CNN
F 1 "+3.3V" H 9500 2340 50  0000 C CNN
F 2 "" H 9500 2200 60  0000 C CNN
F 3 "" H 9500 2200 60  0000 C CNN
	1    9500 2200
	-1   0    0    -1  
$EndComp
$Comp
L C-RESCUE-TDCombat C8
U 1 1 5625347B
P 9050 3900
F 0 "C8" V 9000 3950 50  0000 L CNN
F 1 "100n 100V" V 9100 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9088 3750 30  0001 C CNN
F 3 "" H 9050 3900 60  0000 C CNN
	1    9050 3900
	-1   0    0    1   
$EndComp
$Comp
L C-RESCUE-TDCombat C9
U 1 1 56253559
P 9300 3900
F 0 "C9" V 9250 3950 50  0000 L CNN
F 1 "100n 100V" V 9350 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9338 3750 30  0001 C CNN
F 3 "" H 9300 3900 60  0000 C CNN
	1    9300 3900
	-1   0    0    1   
$EndComp
$Comp
L C-RESCUE-TDCombat C10
U 1 1 562535AE
P 9550 3900
F 0 "C10" V 9500 3950 50  0000 L CNN
F 1 "100n 100V" V 9600 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9588 3750 30  0001 C CNN
F 3 "" H 9550 3900 60  0000 C CNN
	1    9550 3900
	-1   0    0    1   
$EndComp
$Comp
L C-RESCUE-TDCombat C11
U 1 1 56253602
P 9800 3900
F 0 "C11" V 9750 3950 50  0000 L CNN
F 1 "100n 100V" V 9850 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9838 3750 30  0001 C CNN
F 3 "" H 9800 3900 60  0000 C CNN
	1    9800 3900
	-1   0    0    1   
$EndComp
$Comp
L C-RESCUE-TDCombat C12
U 1 1 56253659
P 10050 3900
F 0 "C12" V 10000 3950 50  0000 L CNN
F 1 "10µ 35V" V 10100 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 10088 3750 30  0001 C CNN
F 3 "" H 10050 3900 60  0000 C CNN
	1    10050 3900
	-1   0    0    1   
$EndComp
$Comp
L C-RESCUE-TDCombat C14
U 1 1 56255831
P 8600 5200
F 0 "C14" V 8550 5250 50  0000 L CNN
F 1 "100n 100V" V 8650 5250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 8638 5050 30  0001 C CNN
F 3 "" H 8600 5200 60  0000 C CNN
	1    8600 5200
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-TDCombat D1
U 1 1 56257A08
P 4050 6500
F 0 "D1" H 4250 6550 50  0000 C CNN
F 1 "OPERATIONAL" H 4050 6400 39  0000 C CNN
F 2 "LEDs:LED-0603" H 4050 6500 60  0001 C CNN
F 3 "" H 4050 6500 60  0000 C CNN
	1    4050 6500
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-TDCombat R14
U 1 1 56257B2D
P 4050 6000
F 0 "R14" V 4000 5800 50  0000 C CNN
F 1 "1k" V 4050 6000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3980 6000 30  0001 C CNN
F 3 "" H 4050 6000 30  0000 C CNN
	1    4050 6000
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-TDCombat R16
U 1 1 5627DCA8
P 4350 6000
F 0 "R16" V 4300 5800 50  0000 C CNN
F 1 "1k" V 4350 6000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4280 6000 30  0001 C CNN
F 3 "" H 4350 6000 30  0000 C CNN
	1    4350 6000
	-1   0    0    1   
$EndComp
$Comp
L LED-RESCUE-TDCombat D3
U 1 1 5627DD3B
P 4350 6500
F 0 "D3" H 4550 6550 50  0000 C CNN
F 1 "ERROR" H 4350 6400 39  0000 C CNN
F 2 "LEDs:LED-0603" H 4350 6500 60  0001 C CNN
F 3 "" H 4350 6500 60  0000 C CNN
	1    4350 6500
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-TDCombat R12
U 1 1 5627FEAC
P 1250 3650
F 0 "R12" V 1350 3650 50  0000 C CNN
F 1 "1k" V 1250 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1180 3650 30  0001 C CNN
F 3 "" H 1250 3650 30  0000 C CNN
	1    1250 3650
	0    1    -1   0   
$EndComp
$Comp
L R-RESCUE-TDCombat R13
U 1 1 562804DC
P 1450 3850
F 0 "R13" V 1550 3850 50  0000 C CNN
F 1 "1k8" V 1450 3850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1380 3850 30  0001 C CNN
F 3 "" H 1450 3850 30  0000 C CNN
	1    1450 3850
	-1   0    0    -1  
$EndComp
$Comp
L STM32F446Rx U1
U 1 1 56486E20
P 6700 3150
F 0 "U1" H 6650 5450 60  0000 C CNN
F 1 "STM32F446Rx" H 6700 800 60  0000 C CNN
F 2 "Housings_QFP:LQFP-64_10x10mm_Pitch0.5mm" H 6700 3150 60  0001 C CNN
F 3 "" H 6700 3150 60  0000 C CNN
	1    6700 3150
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-TDCombat C13
U 1 1 5648F3B9
P 8650 4250
F 0 "C13" V 8600 4300 50  0000 L CNN
F 1 "10µ 35V" V 8600 3900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8688 4100 30  0001 C CNN
F 3 "" H 8650 4250 60  0000 C CNN
	1    8650 4250
	-1   0    0    1   
$EndComp
Text HLabel 4850 1300 0    60   Input ~ 0
Ia
Text HLabel 4850 4100 0    60   Input ~ 0
Vdc
Text HLabel 4850 1200 0    60   Input ~ 0
Ib
Text HLabel 4850 1100 0    60   Input ~ 0
Ic
Text HLabel 4850 1000 0    60   Input ~ 0
Temp
Text HLabel 4850 2000 0    60   Output ~ 0
PWM_A
Text HLabel 4850 1900 0    60   Output ~ 0
PWM_B
Text HLabel 4850 1800 0    60   Output ~ 0
PWM_C
Text Label 2050 1550 0    60   ~ 0
HALL_A
Text Label 2050 1650 0    60   ~ 0
HALL_B
Text Label 2050 1750 0    60   ~ 0
HALL_C
Text Label 1750 2700 0    60   ~ 0
DEBUG_TX
Text Label 1750 2800 0    60   ~ 0
DEBUG_RX
Text Label 1950 3650 0    60   ~ 0
PPM_IN
Text Label 4350 5700 1    60   ~ 0
LED_ERROR
Text Label 4050 5700 1    60   ~ 0
LED_OPERATIONAL
Text Label 8650 4050 1    60   ~ 0
VCAP1
$Comp
L +3.3VADC #PWR06
U 1 1 566C7047
P 8600 4800
F 0 "#PWR06" H 8750 4750 50  0001 C CNN
F 1 "+3.3VADC" H 8600 4900 50  0000 C CNN
F 2 "" H 8600 4800 60  0000 C CNN
F 3 "" H 8600 4800 60  0000 C CNN
	1    8600 4800
	1    0    0    -1  
$EndComp
$Comp
L GNDA-RESCUE-TDCombat #PWR07
U 1 1 566C70A5
P 8600 5900
AR Path="/566C70A5" Ref="#PWR07"  Part="1" 
AR Path="/561D5A12/566C70A5" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 8600 5650 50  0001 C CNN
F 1 "GNDA" H 8600 5750 50  0000 C CNN
F 2 "" H 8600 5900 60  0000 C CNN
F 3 "" H 8600 5900 60  0000 C CNN
	1    8600 5900
	1    0    0    -1  
$EndComp
$Comp
L ABM3B-8MHZ X1
U 1 1 56AA89B1
P 9000 1150
F 0 "X1" H 9000 1240 50  0000 C CNN
F 1 "ABM3B-8MHZ" V 9050 450 50  0000 L CNN
F 2 "thunderdrive-combat:ABM3B" H 9000 1150 60  0001 C CNN
F 3 "" H 9000 1150 60  0000 C CNN
	1    9000 1150
	0    -1   -1   0   
$EndComp
Text HLabel 4850 4700 0    60   Input ~ 0
/FAULT
Text Label 1400 800  0    60   ~ 0
DAC_1
Text Label 1400 900  0    60   ~ 0
DAC_2
$Comp
L R-RESCUE-TDCombat R11
U 1 1 56C50930
P 8600 2800
F 0 "R11" V 8700 2800 50  0000 C CNN
F 1 "DNP" V 8600 2800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8530 2800 30  0001 C CNN
F 3 "" H 8600 2800 30  0000 C CNN
	1    8600 2800
	0    1    -1   0   
$EndComp
Wire Wire Line
	9000 1350 9000 1400
Wire Wire Line
	8250 1400 9250 1400
Wire Wire Line
	9650 1150 9150 1150
Wire Wire Line
	9650 900  9650 2750
Wire Wire Line
	9650 900  9550 900 
Connection ~ 9650 1150
Connection ~ 9000 1400
Wire Wire Line
	9000 950  9000 900 
Connection ~ 9000 900 
Wire Wire Line
	8800 900  9250 900 
Wire Wire Line
	9650 1400 9550 1400
Wire Wire Line
	950  1650 3600 1650
Wire Wire Line
	950  1750 3500 1750
Wire Wire Line
	9400 2250 9500 2250
Wire Wire Line
	9500 2250 9500 2200
Wire Wire Line
	9650 2350 9400 2350
Wire Wire Line
	9400 2450 9650 2450
Connection ~ 9650 2450
Wire Wire Line
	9400 2650 9650 2650
Connection ~ 9650 2650
Wire Wire Line
	8850 2650 8900 2650
Connection ~ 9650 1400
Wire Wire Line
	8250 1300 8800 1300
Wire Wire Line
	8800 1300 8800 900 
Wire Wire Line
	8250 2000 8850 2000
Wire Wire Line
	9400 2550 9650 2550
Connection ~ 9650 2550
Wire Wire Line
	9300 4500 9300 4050
Connection ~ 9050 4500
Wire Wire Line
	9300 3600 9300 3750
Connection ~ 9050 3600
Wire Wire Line
	9550 3600 9550 3750
Connection ~ 9300 3600
Wire Wire Line
	9550 4500 9550 4050
Connection ~ 9300 4500
Wire Wire Line
	9800 4500 9800 4050
Connection ~ 9550 4500
Wire Wire Line
	9800 3600 9800 3750
Connection ~ 9550 3600
Connection ~ 9800 3600
Connection ~ 9800 4500
Wire Wire Line
	8600 5350 8600 5900
Connection ~ 8600 5850
Wire Wire Line
	8600 4800 8600 5050
Connection ~ 8600 4900
Wire Wire Line
	8250 3600 10050 3600
Wire Wire Line
	4050 6150 4050 6300
Wire Wire Line
	4050 6700 4050 7150
Wire Wire Line
	4350 6150 4350 6300
Wire Wire Line
	4350 6800 4350 6700
Wire Wire Line
	1100 3550 1100 3450
Wire Wire Line
	950  3750 1100 3750
Wire Wire Line
	1100 3750 1100 4100
Wire Wire Line
	1100 3650 950  3650
Wire Wire Line
	1100 4050 1450 4050
Connection ~ 1100 4050
Wire Wire Line
	1450 4050 1450 4000
Wire Wire Line
	1400 3650 2500 3650
Connection ~ 1450 3650
Wire Wire Line
	8250 4500 10050 4500
Wire Wire Line
	9150 1100 9150 1200
Wire Wire Line
	9150 1100 9100 1100
Wire Wire Line
	9150 1200 9100 1200
Connection ~ 9150 1150
Wire Wire Line
	9050 3400 9050 3750
Wire Wire Line
	8250 3700 8400 3700
Wire Wire Line
	8400 3600 8400 4700
Connection ~ 8400 3600
Wire Wire Line
	8400 3800 8250 3800
Connection ~ 8400 3700
Wire Wire Line
	8400 3900 8250 3900
Connection ~ 8400 3800
Wire Wire Line
	9050 4050 9050 4550
Wire Wire Line
	8400 4700 8250 4700
Connection ~ 8400 3900
Wire Wire Line
	8250 4200 8300 4200
Wire Wire Line
	8300 4200 8300 4500
Connection ~ 8300 4300
Connection ~ 8300 4500
Connection ~ 8300 4400
Wire Wire Line
	8250 4050 8650 4050
Wire Wire Line
	8650 4050 8650 4100
Wire Wire Line
	8650 4400 8650 4500
Connection ~ 8650 4500
Wire Wire Line
	8250 5000 8400 5000
Wire Wire Line
	8400 5000 8400 5850
Connection ~ 9650 2350
Wire Wire Line
	8250 2250 8900 2250
Wire Wire Line
	8250 2350 8900 2350
Wire Wire Line
	8250 2450 8550 2450
Wire Wire Line
	8250 2550 8650 2550
Wire Wire Line
	8650 2550 8650 2450
Wire Wire Line
	8650 2450 8900 2450
Wire Wire Line
	8550 2450 8550 2650
Wire Wire Line
	8550 2650 8750 2650
Wire Wire Line
	8750 2650 8750 2550
Wire Wire Line
	8750 2550 8900 2550
Wire Wire Line
	10050 3600 10050 3750
Wire Wire Line
	10050 4500 10050 4050
Wire Wire Line
	8250 1600 9650 1600
Connection ~ 9650 1600
Wire Wire Line
	8500 1700 8250 1700
Wire Wire Line
	8500 1000 8500 1700
Connection ~ 8500 1600
Wire Wire Line
	5150 1000 4850 1000
Wire Wire Line
	5150 1100 4850 1100
Wire Wire Line
	5150 1200 4850 1200
Wire Wire Line
	5150 1800 4850 1800
Wire Wire Line
	5150 1900 4850 1900
Wire Wire Line
	5150 2000 4850 2000
Wire Wire Line
	4350 5100 4350 5850
Wire Wire Line
	8250 4300 8300 4300
Wire Wire Line
	8250 4400 8300 4400
Wire Wire Line
	1450 3700 1450 3650
Wire Wire Line
	950  3550 1100 3550
Wire Wire Line
	5150 4100 4850 4100
Wire Wire Line
	4850 1300 5150 1300
Wire Wire Line
	950  1550 3700 1550
Wire Wire Line
	5150 1400 4150 1400
Wire Wire Line
	4150 1400 4150 800 
Wire Wire Line
	4150 800  950  800 
Wire Wire Line
	950  900  4050 900 
Wire Wire Line
	4050 900  4050 1500
Wire Wire Line
	4050 1500 5150 1500
Wire Wire Line
	8850 2800 8750 2800
Connection ~ 8850 2650
Text Label 1400 1000 0    60   ~ 0
CAN_RX
Text Label 1400 1100 0    60   ~ 0
CAN_TX
Wire Wire Line
	8250 2650 8350 2650
Wire Wire Line
	8350 2650 8350 2800
Wire Wire Line
	8350 2800 8450 2800
Wire Wire Line
	5150 3400 4850 3400
Wire Wire Line
	5150 3500 4850 3500
Wire Wire Line
	5150 3600 4850 3600
Text HLabel 4850 3400 0    60   Output ~ 0
SCK
Text HLabel 4850 3500 0    60   Input ~ 0
SDO
Text HLabel 4850 3600 0    60   Output ~ 0
SDI
Wire Wire Line
	3700 1550 3700 4400
Wire Wire Line
	3600 1650 3600 4500
Wire Wire Line
	3500 1750 3500 4600
Wire Wire Line
	950  2700 5150 2700
Wire Wire Line
	950  2800 5150 2800
Wire Wire Line
	5150 2600 2500 2600
Wire Wire Line
	2500 2600 2500 3650
Text HLabel 4850 4800 0    60   Output ~ 0
EN_GATE
Wire Wire Line
	5150 3300 4850 3300
Text HLabel 4850 3300 0    60   Output ~ 0
/SCS
Wire Wire Line
	8850 2000 8850 2800
$Comp
L GNDPWR #PWR08
U 1 1 56C38ED8
P 9050 4550
F 0 "#PWR08" H 9050 4350 50  0001 C CNN
F 1 "GNDPWR" H 9050 4420 50  0000 C CNN
F 2 "" H 9050 4500 50  0000 C CNN
F 3 "" H 9050 4500 50  0000 C CNN
	1    9050 4550
	1    0    0    -1  
$EndComp
$Comp
L GNDPWR #PWR09
U 1 1 56C39EBD
P 1100 4100
F 0 "#PWR09" H 1100 3900 50  0001 C CNN
F 1 "GNDPWR" H 1100 3970 50  0000 C CNN
F 2 "" H 1100 4050 50  0000 C CNN
F 3 "" H 1100 4050 50  0000 C CNN
	1    1100 4100
	1    0    0    -1  
$EndComp
$Comp
L GNDPWR #PWR010
U 1 1 56C3A797
P 4050 7150
F 0 "#PWR010" H 4050 6950 50  0001 C CNN
F 1 "GNDPWR" H 4050 7020 50  0000 C CNN
F 2 "" H 4050 7100 50  0000 C CNN
F 3 "" H 4050 7100 50  0000 C CNN
	1    4050 7150
	1    0    0    -1  
$EndComp
$Comp
L GNDPWR #PWR011
U 1 1 56C3ABFD
P 9650 2750
F 0 "#PWR011" H 9650 2550 50  0001 C CNN
F 1 "GNDPWR" H 9650 2620 50  0000 C CNN
F 2 "" H 9650 2700 50  0000 C CNN
F 3 "" H 9650 2700 50  0000 C CNN
	1    9650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4900 8600 4900
Wire Wire Line
	8400 5850 8600 5850
$Comp
L GNDPWR #PWR012
U 1 1 56C824CA
P 5000 5400
F 0 "#PWR012" H 5000 5200 50  0001 C CNN
F 1 "GNDPWR" H 5000 5270 50  0000 C CNN
F 2 "" H 5000 5350 50  0000 C CNN
F 3 "" H 5000 5350 50  0000 C CNN
	1    5000 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2400 5150 2400
Wire Wire Line
	8500 1100 8250 1100
Wire Wire Line
	8250 1000 8500 1000
Connection ~ 8500 1100
Wire Wire Line
	5000 3800 5150 3800
Wire Wire Line
	5000 3900 5150 3900
Connection ~ 5000 3800
Wire Wire Line
	5000 4000 5150 4000
Connection ~ 5000 3900
Wire Wire Line
	5000 4200 5150 4200
Connection ~ 5000 4000
Wire Wire Line
	5000 4300 5150 4300
Connection ~ 5000 4200
Connection ~ 5000 4300
Wire Wire Line
	5000 4900 5150 4900
Wire Wire Line
	4050 6800 4350 6800
Connection ~ 4050 6800
Connection ~ 5000 4900
Wire Wire Line
	5000 5000 5150 5000
Connection ~ 5000 5000
Wire Wire Line
	5000 5100 5150 5100
Connection ~ 5000 5100
Wire Wire Line
	5000 5300 5150 5300
Connection ~ 5000 5300
Wire Wire Line
	3700 4400 5150 4400
Wire Wire Line
	3600 4500 5150 4500
Wire Wire Line
	3500 4600 5150 4600
Wire Wire Line
	5000 3100 5150 3100
Connection ~ 5000 3100
Wire Wire Line
	4850 4700 5150 4700
Wire Wire Line
	5150 2500 5000 2500
Connection ~ 5000 2500
Wire Wire Line
	4150 5100 4350 5100
Wire Wire Line
	4850 4800 5150 4800
$Comp
L R-RESCUE-TDCombat R3
U 1 1 56CCF5CA
P 1450 2600
F 0 "R3" V 1550 2600 50  0000 C CNN
F 1 "DNP" V 1450 2600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1380 2600 30  0001 C CNN
F 3 "" H 1450 2600 30  0000 C CNN
	1    1450 2600
	0    1    -1   0   
$EndComp
Wire Wire Line
	1300 2600 1200 2600
Wire Wire Line
	1200 2600 1200 2800
Connection ~ 1200 2800
Wire Wire Line
	1600 2600 1700 2600
Wire Wire Line
	1700 2600 1700 2700
Connection ~ 1700 2700
Connection ~ 5000 2400
Wire Wire Line
	3950 1000 3950 2900
Wire Wire Line
	3950 1000 950  1000
Wire Wire Line
	3850 1100 3850 3000
Wire Wire Line
	3850 1100 950  1100
Wire Wire Line
	3950 2900 5150 2900
Wire Wire Line
	3850 3000 5150 3000
Wire Wire Line
	5150 2100 5000 2100
Wire Wire Line
	5150 2200 5000 2200
Connection ~ 5000 2200
Wire Wire Line
	5000 2100 5000 5400
Wire Wire Line
	5150 1600 4050 1600
Wire Wire Line
	4050 1600 4050 5850
Wire Wire Line
	5150 1700 4150 1700
Wire Wire Line
	4150 1700 4150 5100
$Comp
L +5V #PWR013
U 1 1 5627F838
P 1100 3450
F 0 "#PWR013" H 1100 3300 50  0001 C CNN
F 1 "+5V" H 1100 3590 50  0000 C CNN
F 2 "" H 1100 3450 60  0000 C CNN
F 3 "" H 1100 3450 60  0000 C CNN
	1    1100 3450
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X01 P17
U 1 1 56D8A60B
P 750 3650
F 0 "P17" H 900 3650 50  0000 C CNN
F 1 "PPM" H 500 3700 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 3650 50  0001 C CNN
F 3 "" H 750 3650 50  0000 C CNN
	1    750  3650
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P16
U 1 1 56D8A6D4
P 750 3550
F 0 "P16" H 900 3550 50  0000 C CNN
F 1 "5V" H 550 3600 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 3550 50  0001 C CNN
F 3 "" H 750 3550 50  0000 C CNN
	1    750  3550
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P18
U 1 1 56D8A726
P 750 3750
F 0 "P18" H 900 3750 50  0000 C CNN
F 1 "GND" H 500 3800 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 3750 50  0001 C CNN
F 3 "" H 750 3750 50  0000 C CNN
	1    750  3750
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P14
U 1 1 56D8A783
P 750 2800
F 0 "P14" H 900 2800 50  0000 C CNN
F 1 "RX" H 500 2850 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 2800 50  0001 C CNN
F 3 "" H 750 2800 50  0000 C CNN
	1    750  2800
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P13
U 1 1 56D8AA4B
P 750 2700
F 0 "P13" H 900 2700 50  0000 C CNN
F 1 "TX" H 500 2750 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 2700 50  0001 C CNN
F 3 "" H 750 2700 50  0000 C CNN
	1    750  2700
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P10
U 1 1 56D8AAA6
P 750 1550
F 0 "P10" H 900 1550 50  0000 C CNN
F 1 "HALLA" H 400 1600 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 1550 50  0001 C CNN
F 3 "" H 750 1550 50  0000 C CNN
	1    750  1550
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P11
U 1 1 56D8AC86
P 750 1650
F 0 "P11" H 900 1650 50  0000 C CNN
F 1 "HALLB" H 400 1700 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 1650 50  0001 C CNN
F 3 "" H 750 1650 50  0000 C CNN
	1    750  1650
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P12
U 1 1 56D8ACE7
P 750 1750
F 0 "P12" H 900 1750 50  0000 C CNN
F 1 "HALLC" H 400 1800 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 1750 50  0001 C CNN
F 3 "" H 750 1750 50  0000 C CNN
	1    750  1750
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P9
U 1 1 56D8AD4B
P 750 1100
F 0 "P9" H 900 1100 50  0000 C CNN
F 1 "CANTX" H 450 1150 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 1100 50  0001 C CNN
F 3 "" H 750 1100 50  0000 C CNN
	1    750  1100
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P8
U 1 1 56D8ADD4
P 750 1000
F 0 "P8" H 900 1000 50  0000 C CNN
F 1 "CANRX" H 450 1050 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 1000 50  0001 C CNN
F 3 "" H 750 1000 50  0000 C CNN
	1    750  1000
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P7
U 1 1 56D8AE3E
P 750 900
F 0 "P7" H 900 900 50  0000 C CNN
F 1 "DAC2" H 450 950 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 900 50  0001 C CNN
F 3 "" H 750 900 50  0000 C CNN
	1    750  900 
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P6
U 1 1 56D8AEAF
P 750 800
F 0 "P6" H 900 800 50  0000 C CNN
F 1 "DAC1" H 450 850 50  0000 C CNN
F 2 "thunderdrive-combat:SMD_PAD_Small" H 750 800 50  0001 C CNN
F 3 "" H 750 800 50  0000 C CNN
	1    750  800 
	-1   0    0    1   
$EndComp
$EndSCHEMATC
