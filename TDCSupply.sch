EESchema Schematic File Version 2
LIBS:TDCombat-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:ThunderDrive
LIBS:crf_1
LIBS:con-amp-micromatch
LIBS:TDCombat-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Thunderdrive Combat"
Date "2016-02-15"
Rev "2"
Comp "Thunder Tecnologies"
Comment1 "BEC power supply"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R-RESCUE-TDCombat R24
U 1 1 56259E5C
P 2650 3450
F 0 "R24" V 2730 3450 50  0000 C CNN
F 1 "23k2" V 2650 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2580 3450 30  0001 C CNN
F 3 "" H 2650 3450 30  0000 C CNN
	1    2650 3450
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-TDCombat C35
U 1 1 56259E8D
P 2450 3500
F 0 "C35" V 2400 3550 50  0000 L CNN
F 1 "10µ 100V" V 2500 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210_HandSoldering" H 2488 3350 30  0001 C CNN
F 3 "" H 2450 3500 60  0000 C CNN
	1    2450 3500
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky D5
U 1 1 5625A100
P 4800 3500
F 0 "D5" H 4800 3600 50  0000 C CNN
F 1 "B560" H 4800 3400 50  0000 C CNN
F 2 "Diodes_SMD:SMC_Handsoldering" H 4800 3500 60  0001 C CNN
F 3 "" H 4800 3500 60  0000 C CNN
	1    4800 3500
	0    1    1    0   
$EndComp
$Comp
L C-RESCUE-TDCombat C36
U 1 1 5625A7A0
P 2900 3500
F 0 "C36" H 2925 3600 50  0000 L CNN
F 1 "10n 25V" H 2925 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2938 3350 30  0001 C CNN
F 3 "" H 2900 3500 60  0000 C CNN
	1    2900 3500
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-TDCombat C32
U 1 1 5625A9E7
P 4450 2850
F 0 "C32" V 4500 2900 50  0000 L CNN
F 1 "100n 50V" V 4400 2900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4488 2700 30  0001 C CNN
F 3 "" H 4450 2850 60  0000 C CNN
	1    4450 2850
	0    1    1    0   
$EndComp
$Comp
L R-RESCUE-TDCombat R22
U 1 1 5625AEBD
P 5600 3050
F 0 "R22" V 5680 3050 50  0000 C CNN
F 1 "68k" V 5600 3050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5530 3050 30  0001 C CNN
F 3 "" H 5600 3050 30  0000 C CNN
	1    5600 3050
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-TDCombat R25
U 1 1 5625AF80
P 5600 3450
F 0 "R25" V 5680 3450 50  0000 C CNN
F 1 "12k" V 5600 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5530 3450 30  0001 C CNN
F 3 "" H 5600 3450 30  0000 C CNN
	1    5600 3450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR033
U 1 1 5625B5CB
P 6050 2700
F 0 "#PWR033" H 6050 2550 50  0001 C CNN
F 1 "+5V" H 6050 2840 50  0000 C CNN
F 2 "" H 6050 2700 60  0000 C CNN
F 3 "" H 6050 2700 60  0000 C CNN
	1    6050 2700
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-TDCombat C37
U 1 1 5626B1E1
P 9200 3350
F 0 "C37" H 9225 3450 50  0000 L CNN
F 1 "47µ 25V" H 9225 3250 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 9238 3200 30  0001 C CNN
F 3 "" H 9200 3350 60  0000 C CNN
	1    9200 3350
	1    0    0    -1  
$EndComp
$Comp
L GNDA-RESCUE-TDCombat #PWR034
U 1 1 5626CA07
P 9600 3900
F 0 "#PWR034" H 9600 3650 50  0001 C CNN
F 1 "GNDA" H 9600 3750 50  0000 C CNN
F 2 "" H 9600 3900 60  0000 C CNN
F 3 "" H 9600 3900 60  0000 C CNN
	1    9600 3900
	1    0    0    -1  
$EndComp
$Comp
L +3.3VADC #PWR035
U 1 1 5626CA47
P 9600 2700
F 0 "#PWR035" H 9750 2650 50  0001 C CNN
F 1 "+3.3VADC" H 9600 2800 50  0000 C CNN
F 2 "" H 9600 2700 60  0000 C CNN
F 3 "" H 9600 2700 60  0000 C CNN
	1    9600 2700
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-TDCombat C34
U 1 1 5628A6CC
P 8250 3400
F 0 "C34" H 8000 3500 50  0000 L CNN
F 1 "10n 6V" H 7950 3300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 8288 3250 30  0001 C CNN
F 3 "" H 8250 3400 60  0000 C CNN
	1    8250 3400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V-RESCUE-TDCombat #PWR036
U 1 1 5628A6E2
P 8250 2800
F 0 "#PWR036" H 8250 2650 50  0001 C CNN
F 1 "+3.3V" H 8250 2940 50  0000 C CNN
F 2 "" H 8250 2800 60  0000 C CNN
F 3 "" H 8250 2800 60  0000 C CNN
	1    8250 2800
	1    0    0    -1  
$EndComp
Text HLabel 2300 3750 0    60   Input ~ 0
Bat-
$Comp
L PWR_FLAG #FLG037
U 1 1 56327837
P 3000 2300
F 0 "#FLG037" H 3000 2395 50  0001 C CNN
F 1 "PWR_FLAG" H 3000 2480 50  0000 C CNN
F 2 "" H 3000 2300 60  0000 C CNN
F 3 "" H 3000 2300 60  0000 C CNN
	1    3000 2300
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG038
U 1 1 563389BD
P 5600 2750
F 0 "#FLG038" H 5600 2845 50  0001 C CNN
F 1 "PWR_FLAG" H 5600 2930 50  0000 C CNN
F 2 "" H 5600 2750 60  0000 C CNN
F 3 "" H 5600 2750 60  0000 C CNN
	1    5600 2750
	1    0    0    -1  
$EndComp
$Comp
L LMR14030 U4
U 1 1 5636D22F
P 3650 3050
F 0 "U4" H 3800 3500 60  0000 C CNN
F 1 "LMR14030" H 4050 2600 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-8-1EP_3.9x4.9mm_Pitch1.27mm" H 3650 3050 60  0001 C CNN
F 3 "" H 3650 3050 60  0000 C CNN
	1    3650 3050
	1    0    0    -1  
$EndComp
$Comp
L TPS732xx U3
U 1 1 565391C1
P 7500 2950
F 0 "U3" H 7500 3200 60  0000 C CNN
F 1 "TPS73233" H 7750 2650 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 7500 2950 60  0001 C CNN
F 3 "" H 7500 2950 60  0000 C CNN
	1    7500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 3650 2450 3850
Wire Wire Line
	3000 2850 3100 2850
Wire Wire Line
	3000 2300 3000 2850
Connection ~ 3000 2400
Wire Wire Line
	3100 3050 2650 3050
Wire Wire Line
	2650 3050 2650 3300
Wire Wire Line
	2650 3750 2650 3600
Connection ~ 2650 3750
Wire Wire Line
	2900 3750 2900 3650
Connection ~ 2900 3750
Wire Wire Line
	2900 3350 2900 3250
Wire Wire Line
	2900 3250 3100 3250
Wire Wire Line
	4200 2850 4300 2850
Wire Wire Line
	4200 3050 4800 3050
Wire Wire Line
	4800 2850 4800 3350
Connection ~ 4800 3050
Wire Wire Line
	4600 2850 4950 2850
Wire Wire Line
	4800 3750 4800 3650
Connection ~ 4800 2850
Wire Wire Line
	4200 3250 5600 3250
Wire Wire Line
	5600 3200 5600 3300
Connection ~ 5600 3250
Wire Wire Line
	5600 2750 5600 2900
Wire Wire Line
	5600 3750 5600 3600
Connection ~ 4800 3750
Wire Wire Line
	6050 2700 6050 3250
Connection ~ 5600 2850
Wire Wire Line
	6050 3750 6050 3550
Connection ~ 5600 3750
Connection ~ 6050 2850
Wire Wire Line
	5450 2850 6900 2850
Wire Wire Line
	7500 3750 7500 3350
Wire Wire Line
	8250 3750 8250 3550
Wire Wire Line
	2450 2300 2450 3350
Wire Wire Line
	3750 3750 3750 3650
Wire Wire Line
	3550 3650 3550 3850
Wire Wire Line
	8100 3050 8250 3050
Wire Wire Line
	8250 3050 8250 3250
Wire Wire Line
	6900 3050 6850 3050
Connection ~ 6850 2850
Wire Wire Line
	8250 2800 8250 2850
Wire Wire Line
	3650 2400 3650 2450
Connection ~ 2450 2400
$Comp
L C-RESCUE-TDCombat C33
U 1 1 56A94D36
P 6050 3400
F 0 "C33" V 6000 3450 50  0000 L CNN
F 1 "47µ 25V" V 6100 3450 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 6088 3250 30  0001 C CNN
F 3 "" H 6050 3400 60  0000 C CNN
	1    6050 3400
	1    0    0    -1  
$EndComp
Connection ~ 6050 3750
Connection ~ 7500 3750
Connection ~ 2450 3750
Wire Wire Line
	2300 3750 8500 3750
$Comp
L L L1
U 1 1 56BF4457
P 5200 2850
F 0 "L1" V 5280 2850 50  0000 C CNN
F 1 "7µ8" V 5100 2850 50  0000 C CNN
F 2 "thunderdrive-combat:Choke_SMD_WE_HCI" H 5200 2850 60  0001 C CNN
F 3 "" H 5200 2850 60  0000 C CNN
	1    5200 2850
	0    1    1    0   
$EndComp
$Comp
L L L2
U 1 1 56BF4591
P 8750 2850
F 0 "L2" V 8830 2850 50  0000 C CNN
F 1 "1k 100MHz" V 8650 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8750 2850 60  0001 C CNN
F 3 "" H 8750 2850 60  0000 C CNN
	1    8750 2850
	0    -1   -1   0   
$EndComp
$Comp
L +24V #PWR039
U 1 1 56BF4B1A
P 2450 2300
F 0 "#PWR039" H 2450 2150 50  0001 C CNN
F 1 "+24V" H 2450 2440 50  0000 C CNN
F 2 "" H 2450 2300 50  0000 C CNN
F 3 "" H 2450 2300 50  0000 C CNN
	1    2450 2300
	1    0    0    -1  
$EndComp
Text HLabel 2300 2400 0    60   Input ~ 0
Bat+
Wire Wire Line
	2300 2400 3650 2400
$Comp
L PWR_FLAG #FLG040
U 1 1 56BF5A79
P 2450 3850
F 0 "#FLG040" H 2450 3945 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 4030 50  0000 C CNN
F 2 "" H 2450 3850 60  0000 C CNN
F 3 "" H 2450 3850 60  0000 C CNN
	1    2450 3850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 3050 6850 2850
$Comp
L L L3
U 1 1 56BF878C
P 8750 3750
F 0 "L3" V 8830 3750 50  0000 C CNN
F 1 "1k 100MHz" V 8650 3750 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 8750 3750 60  0001 C CNN
F 3 "" H 8750 3750 60  0000 C CNN
	1    8750 3750
	0    -1   -1   0   
$EndComp
Connection ~ 3550 3750
Connection ~ 3750 3750
Text Notes 4950 2700 0    39   ~ 0
Wurth Elektronik\n744325780
$Comp
L GNDPWR #PWR041
U 1 1 56C2D0E1
P 3550 3850
F 0 "#PWR041" H 3550 3650 50  0001 C CNN
F 1 "GNDPWR" H 3550 3720 50  0000 C CNN
F 2 "" H 3550 3800 50  0000 C CNN
F 3 "" H 3550 3800 50  0000 C CNN
	1    3550 3850
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG042
U 1 1 56C2E242
P 9200 3900
F 0 "#FLG042" H 9200 3995 50  0001 C CNN
F 1 "PWR_FLAG" H 9200 4080 50  0000 C CNN
F 2 "" H 9200 3900 60  0000 C CNN
F 3 "" H 9200 3900 60  0000 C CNN
	1    9200 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	8100 2850 8500 2850
$Comp
L PWR_FLAG #FLG043
U 1 1 56C3C22D
P 9200 2700
F 0 "#FLG043" H 9200 2795 50  0001 C CNN
F 1 "PWR_FLAG" H 9200 2880 50  0000 C CNN
F 2 "" H 9200 2700 50  0000 C CNN
F 3 "" H 9200 2700 50  0000 C CNN
	1    9200 2700
	1    0    0    -1  
$EndComp
Connection ~ 8250 2850
Connection ~ 8250 3750
Wire Wire Line
	9000 2850 9600 2850
Wire Wire Line
	9200 2700 9200 3200
Wire Wire Line
	9600 2850 9600 2700
Connection ~ 9200 2850
Wire Wire Line
	9200 3500 9200 3900
Wire Wire Line
	9000 3750 9600 3750
Connection ~ 9200 3750
Wire Wire Line
	9600 3750 9600 3900
Text Notes 2300 3100 3    39   ~ 0
C5750X7S2A106M
$EndSCHEMATC
